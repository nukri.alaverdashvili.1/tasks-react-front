import React, {Component, useState} from 'react';
import { Drawer } from 'react-rainbow-components';
import "bootstrap/dist/css/bootstrap.min.css"
import Register from './register'

export default class sideregister extends Component {
    constructor(props){ 
        super(props);
    
        this.state = { //სტეიტს ვიყენებთ იმისთვის რომ შევქმნათ ვარიაბლე
            isOpenRight: false,
        }
    }
    render() {
        return (
            <div>
            <div className="rainbow-flex rainbow-flex_row">
                <div className="rainbow-align-content_center rainbow-p-medium rainbow-m_auto">
                <button className="btn btn-dark" onClick={() => this.setState({
                            isOpenRight: true,
                        })}> Register </button>
                </div>
            </div>
            <Drawer
                header={<Register/>}
                slideFrom="right"
                isOpen={this.state.isOpenRight}
                onRequestClose={() => this.setState({ isOpenRight : false })}
            />
        </div>
        )
    }
}
