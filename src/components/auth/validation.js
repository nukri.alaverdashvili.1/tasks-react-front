const validation =(values)=>{
    let errors = {};

    if(values.name.length <5)
    {
        errors.name = 'Name must be minimum 5 charachter'
    }else if (!values.name)
    {
        errors.name = 'Names is Required'
    }

    if(values.password =='')
    {
        errors.password = 'Password is required'
    }else if (values.password.length <5)
    {
        errors.password = 'Password must be minimum 5 Character'
    }

    if(errors.password || errors.name)
    {
        return errors;
    }
    return false;
}

export default validation;