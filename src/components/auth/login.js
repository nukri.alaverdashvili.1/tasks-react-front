import React, {useState} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Loginimg from '../../images/Login.svg'
import Cookies from 'js-cookie'

const inputStyles = {
    width: 500,
    marginTop:25,
};

function Login() {
    const [email, setEmail]=useState('')
    const [password, setPassword]=useState('')
    const [error, setError]=useState(false)

    async function signUp(e)
    {
        e.preventDefault();
        const result = await axios.post('/login',{email, password})
        if(result.data.msg)
        {
            setError(result.data.msg) // თუ სერვერიდან მივიღებთ msg ს მასინ ერორებში შევინახავთ
        }else{  
            Cookies.set('token', result.data.token) //თუ ერორები არ გვაქვს cook ში ვინახავთ სერვერიდან წამოსულ ტოკენს
            window.location = '/profile'; //გადაამისამართებს პროფილზე
        }
    }
    return (
        <div>
       <img className="img-fluid mb-5" src={Loginimg}></img>   
       {error && <div className="alert alert-danger" role="alert">
           <h5>{error}</h5>
       </div>
        }
        <form onSubmit={signUp} >
            <Input
                placeholder="Input Email"
                type="email"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
                />

            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
                <input style={{marginLeft:'125px'}} type="submit" className="btn btn-primary align-content-center mt-5"/>    
            </form>
        </div>
    )
}

export default Login
