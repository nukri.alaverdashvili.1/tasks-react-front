import React, {useState, useEffect} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Registerimg from '../../images/Register.svg'
import Validation from './validation'

const inputStyles = {
    width: 500,
    marginTop:25,
};

function Register() {
    const [name, setName]=useState('')
    const [password, setPassword]=useState('')
    const [email, setEmail]=useState('')
    const [error, setError]=useState(false)
    const [success, setSuccess] = useState(false)

    async function signUp(e) //ფუნქცია გამოიძახება რეგისტრაციის ღილაკს დააჭერენ ხელს
    {
        e.preventDefault();
        let item={name,password,email} //ყველა ცვლადს რომელიც ზემოთ აღვწერეთ ვინახავთ item ში

        let check = Validation(item) //ვუკეთებთ ითემებს ვალიდაციას, რომელიც ცალკე ფუნქციაა
        if(!check) //თუ ვალიდაციის შემდეგ ფალსი დაბრუნდება
        {
            const result = await axios.post('/saveUser',{name, password, email})
            if(result.data.msg)
            {
                console.log(result.data.msg)
                setError({msg:result.data.msg})
            }else{
                setSuccess('Successfuly Registrierd Please Logged In')
                window.setTimeout(function(){
                    window.location = '/'
                },1000)
            }
        }else{
            setError(check) //ერორის შემთხვევაში 
        }
    }
    return (
        <div>
       <img className="img-fluid mb-5" src={Registerimg}></img>   
       {error &&
       <>
       {error.name && <div className="alert alert-danger" role="alert"> {/* თუ ნეიმის ერორია მაშგ შემთხვევაში გამოიტანს მხოლოდ ამ შეტყობინებას */}
           <h5>{error.name}</h5>
       </div>
        }
        {error.password && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.password}</h5>
       </div>
        }
         {error.msg && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.msg}</h5>
       </div>
        }
        </>
        }

        {!error && success && <div className="alert alert-success" role="alert">{/* თუ ერორები არ არის და საქსესი თრუ არის მაგ შემთხვევაში გამოიტანს საქსესის მესიჯს */}
                <h5> {success} </h5>
        </div>}

        <form onSubmit={signUp} >
            <Input
                placeholder="Name"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={name}  //ამ ინფუთის მნიშვნელობა არის ზემოთ აღნიშნული ნეიმი თავიდან ცარიელი სტრინგი
                onChange={(e)=>setName(e.target.value)} // ინფუთში რამის ჩაწერის შემდეგ ნეიმი შეიცვლება და გახდება ის მნიშვნელობა რასაც ინფუთში ჩავწერთ
                />

            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
            <Input
                placeholder="inputEmail@gmail.com"
                type="email"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
            />
            <input style={{marginLeft:'125px'}} type="submit" className="btn btn-primary align-content-center mt-5"/>    
            </form>
        </div>
    )
}

export default Register
