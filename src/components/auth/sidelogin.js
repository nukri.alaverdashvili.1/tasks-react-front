import React, {Component, useState} from 'react';
import { Drawer } from 'react-rainbow-components';
import "bootstrap/dist/css/bootstrap.min.css"
import Login from './login'

export default class sidecreate extends Component {
    constructor(props){ 
        super(props);
    
        this.state = { //სტეიტს ვიყენებთ იმისთვის რომ შევქმნათ ვარიაბლე
            isOpenRight: false,
        }
    }
    render() {
        return (
            <div>
            <div className="rainbow-flex rainbow-flex_row">
                <div className="rainbow-align-content_center rainbow-p-medium rainbow-m_auto">
                <button className="btn btn-dark" onClick={() => this.setState({
                            isOpenRight: true,
                        })}> Login </button>
                </div>
            </div>
            <Drawer
                header={<Login/>}
                slideFrom="right"
                isOpen={this.state.isOpenRight}
                onRequestClose={() => this.setState({ isOpenRight : false })}
            />
        </div>
        )
    }
}
