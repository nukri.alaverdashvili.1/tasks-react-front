import {React, useState} from 'react'
import  { Redirect } from 'react-router-dom'
import Welcome from './welcome'

function log({error, loginData,handleLogin,handleFailure}) {
    return (
        <div>
         {
          loginData ? ( //თუ ლოგინდატა ანუ იუზერის მონაცემები არსებობს მაშინ გადამისამართეს პროფილზე
            <Redirect to='/profile' ></Redirect>
          )
          :( //თუ არ არის მაშინ გამოიტანოს ლოგინის ფუნქცია
            <Welcome error={error} handleLogin={handleLogin} handleFailure={handleFailure}></Welcome>
            )
        }
        </div>
    )
}

export default log
