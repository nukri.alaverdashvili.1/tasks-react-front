import React, {useState, useEffect} from 'react'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import {Link} from 'react-router-dom'
import {SidebarData} from './sidebarData'
import "bootstrap/dist/css/bootstrap.min.css"
import './nav.css';
import jwt_decode from "jwt-decode";
import Cookies from 'js-cookie'

function Nav({logout}) {
    const [sidebar, setSidebar]=useState(true)
    const data = jwt_decode(Cookies.get('token'))
    const showSidebar = ()=>{
        setSidebar(!sidebar)
    }

    return (
        <>
        <div>
            <Link to="#" className="menu-bars">
                <FaIcons.FaBars onClick={showSidebar} size={25} style={{color:'black'}}/> {/*ღილაკზე დაჭერისას ნავაიგაციის გამოჩენა */}
            </Link>
        </div>

        <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
            <ul className='nav-menu-items' >
                <li className='navbar-toggle'>
                    <Link to='#' className='menu-bars'>
                    <AiIcons.AiOutlineClose onClick={showSidebar} size={25} style={{color:'white'}}/>
                    </Link>
                </li>
                <li className='navbar-toggle' style={{color:'white'}}> {data.email} </li>
                {SidebarData.map((item,index)=>
                {
                    return (
                        (() => {
                            if (item.title =='LogOut') {
                              return (
                            <li key={index} className={item.cName}>
                                <button className='btn btn-close-white' onClick={logout}> 
                                {item.icon}
                                <span>{item.title}</span>
                                </button>
                             </li>
                              )
                            } else {
                              return (
                                <li key={index} className={item.cName}>
                                <Link to={item.path}> 
                                {item.icon}
                                <span>{item.title}</span>
                                </Link>
                             </li>
                            )
                            }
                          })()
                          
                    )
                })}
            </ul>
        </nav>
        </>
    )
}

export default Nav
