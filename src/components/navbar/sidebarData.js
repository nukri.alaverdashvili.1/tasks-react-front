import React from 'react'
import * as FaIcons from 'react-icons/fa'
import * as AiIcons from 'react-icons/ai'
import * as IoIcons from 'react-icons/io'
import * as RiIcons from 'react-icons/ri'


export const SidebarData =[
    {
        title:'My Profile',
        path:'/profile',
        icon:<AiIcons.AiFillHome />, 
        cName: 'nav-text'
    },
    {
        title:'Tasks',
        path:'/tasks',
        icon:<IoIcons.IoIosPaper />, 
        cName: 'nav-text'
    },
    {
        title:'Create Task',
        path:'/create',
        icon:<FaIcons.FaCartPlus />, 
        cName: 'nav-text'
    },
    {
        title:'LogOut',
        path:'/logout',
        icon:<RiIcons.RiLogoutCircleRLine />, 
        cName: 'nav-text'
    },
]