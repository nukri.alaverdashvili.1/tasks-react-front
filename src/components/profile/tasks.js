import React, { Component, useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import axios from "axios";
import { FcOk, FcTodoList, FcCancel } from "react-icons/fc";
import { AiOutlineHourglass } from "react-icons/ai";
import { ButtonIcon } from "react-rainbow-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Todo from "../../images/Todo.svg";
import Inprogress from "../../images/Inprogress.svg";
import Done from "../../images/Done.svg";
import Blocked from "../../images/Blocked.svg";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import Sideeffect from "./sideeffect";

function Tasks({tasks, statuses, deleteTask, editTask}) {
    const [error, setError] = useState(false)

    const deleteTasks = async(id)=>{
            let data = await axios.delete(`/profile/delete/${id}`).then(()=>{
              deleteTask(id)        
            }).catch((err)=>{
              setError(err.message)
            })
    }
    
    const checkStatus = (id)=>{
      const tree = statuses.filter((status)=>{
        return status._id === id
      })
      return tree[0]
    }

    const setIcon = (status)=>{
            if(status == 'to-do')
            {
               return <FcTodoList size={25} />
            }else if(status == 'in progress'){
                return<AiOutlineHourglass size={25} style={{color:'red'}}/>
            }else if (status =='blocked')
            {
               return <FcCancel size={25} style={{color:'red'}} />
            }else{
                return <FcOk size={25} />
            }
    }

  return (
    <div className="row">
        {error && (
          <div className="alert alert-danger" role="alert">
            <h5>{error}</h5>
          </div>
        )}
      <div className="col-lg-6 col-sm-12 col-xl-3">
        <img
          style={{ height: "150px" }}
          className="img-fluid mb-5"
          src={Todo}
        ></img>
        <div className="card mt-5">
          {tasks.map((task) => {
            const status = checkStatus(task.status)
            if (status && status.status && status.status == "to-do") {
              return (
                <div key={task._id} className="card-body">
                  <h5 key={task.task} className="card-title">
                    Task: {task.task}
                  </h5>
                  <p key={task.date} className="card-text">
                    Date: {task.date.substring(0, 10)}
                  </p>
                  <h5 key={task.status} className="card-title">
                    Status: {status.status} {setIcon(status.status)}
                  </h5>
                  <div className="d-flex">
                  <Sideeffect id={task._id} editTask={editTask} profileStatus={status.status} type='edit'/>
                      <ButtonIcon
                        onClick={() => deleteTasks(task._id)}
                        variant="border-filled"
                        size="medium"
                        tooltip="Delete"
                        icon={<FontAwesomeIcon icon={faTrashAlt} />}
                      />
                  </div>
                </div>
              );
            }
          })}
        </div>
      </div>

      <div className="col-lg-6 col-sm-12 col-xl-3">
        <img
          style={{ height: "150px" }}
          className="img-fluid mb-5"
          src={Inprogress}
        ></img>
        <div className="card mt-5">
          {tasks.map((task) => {
            const status = checkStatus(task.status)
            if (status && status.status && status.status == "in progress") {
              return (
                <div key={task._id} className="card-body">
                  <h5 key={task.task} className="card-title">
                    Task: {task.task}
                  </h5>
                  <p key={task.date} className="card-text">
                    Date: {task.date.substring(0, 10)}
                  </p>
                  <h5 key={task.status} className="card-title">
                    Status: {status.status} {setIcon(status.status)}
                  </h5>
                  <div className="d-flex">
                  <Sideeffect id={task._id} editTask={editTask} profileStatus={status.status} type='edit'/>
                      {" "}
                      <ButtonIcon
                        onClick={() => deleteTasks(task._id)}
                        variant="border-filled"
                        size="medium"
                        tooltip="Delete"
                        icon={<FontAwesomeIcon icon={faTrashAlt} />}
                      />{" "}
                  </div>
                </div>
              );
            }
          })}
        </div>
      </div>

      <div className="col-lg-6 col-sm-12 col-xl-3">
        <img
          style={{ height: "150px" }}
          className="img-fluid mb-5"
          src={Blocked}
        ></img>
        <div className="card mt-5">
          {tasks.map((task) => {
            const status = checkStatus(task.status)
            if (status && status.status && status.status == "blocked") {
              return (
                <div key={task._id} className="card-body">
                  <h5 key={task.task} className="card-title">
                    Task: {task.task}
                  </h5>
                  <p key={task.date} className="card-text">
                    Date: {task.date.substring(0, 10)}
                  </p>
                  <h5 key={task.status} className="card-title">
                    Status: {status.status} {setIcon(status.status)}
                  </h5>
                  <div className="d-flex">
                  <Sideeffect id={task._id} editTask={editTask} profileStatus={status.status} type='edit'/>
                      {" "}
                      <ButtonIcon
                        onClick={() => deleteTasks(task._id)}
                        variant="border-filled"
                        size="medium"
                        tooltip="Delete"
                        icon={<FontAwesomeIcon icon={faTrashAlt} />}
                      />{" "}
                  </div>
                </div>
              );
            }
          })}
        </div>
      </div>

      <div className="col-lg-6 col-sm-12 col-xl-3">
        <img
          style={{ height: "150px" }}
          className="img-fluid mb-5"
          src={Done}
        ></img>
        <div className="card mt-5">
          {tasks.map((task) => {
            const status = checkStatus(task.status)
            if (status && status.status && status.status == "done") {
            return (
                <div key={task._id} className="card-body">
                  <h5 key={task.task} className="card-title">
                    Task: {task.task}
                  </h5>
                  <p key={task.date} className="card-text">
                    Date: {task.date.substring(0, 10)}
                  </p>
                  <h5 key={task.status} className="card-title">
                    Status: {status.status} {setIcon(status.status)}
                  </h5>
                  <div className="d-flex">
                  <Sideeffect id={task._id} editTask={editTask} profileStatus={status.status} type='edit'/>
                      {" "}
                      <ButtonIcon
                        onClick={() => deleteTasks(task._id)}
                        variant="border-filled"
                        size="medium"
                        tooltip="Delete"
                        icon={<FontAwesomeIcon icon={faTrashAlt} />}
                      />{" "}
                  </div>
                </div>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
}

export default Tasks;
