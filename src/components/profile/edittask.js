import React, { Component, useState, useEffect } from "react";
import { DatePicker } from "react-rainbow-components";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
import { Input, Select } from "react-rainbow-components";
import Process from "../../images/Process.svg";
import "bootstrap/dist/css/bootstrap.min.css";

const inputStyles = {
  width: 500,
};

const Edittask = ({id, closeSide, editTask, profileStatus}) => {
  const [task, setTask] = useState("");
  const [date, setDate] = useState(new Date());
  const [status, setStatus] = useState("");
  const [statuses, setStatuses] = useState([]);
  const [error, setError] = useState([]);
  const [success, setSuccess] = useState(false);

  const [tasksLoading, setTasksLoading] = useState(false);
  console.log(profileStatus)
      useEffect(async () =>{ 
            await axios.get('/status') //იუზეფექტის გამოყენებით ყოველ ჩატვირთვაზე წამოვიღებ მონაცემებს ბაზიდან ტასკის სტატუსებზე
            .then((response) =>{
                if(response.data.length>0){ //თუ წამოღებული ინფორმაციის სიგრძე 0 ზე მეტი იქნება მაშინ შემოდის იფში
                    setStatuses({statuses:response.data.map(status => status.status)}) //სტატუსებს ვააფდეითებთ და შიგნით ვწერთ ბაზიდან წამოღებულ სტატუსებს
                }
            }).catch(()=>{
              setError({msg:'Server Error 505'})
            })

           await axios.get(`/profile/getTask/${id}`)
           .then((response) =>{
                let date = JSON.stringify(response.data.date)
                date = date.substring(1, 11)
                if(response.data._id){
                    setTask(response.data.task)
                    setDate(response.data.date)
                    setStatus(profileStatus)
                }
            }).catch(()=>{
                setError({msg:'Server Error 505'})
            }).finally(()=>setTasksLoading(false))

    },[])

    async function onSubmit(e)
    {
        e.preventDefault();

        let tasks = {task, date, status}
        if(task == '')
        {
          setError({msg:'TaskName is required'}) //თუ ტასკში არაფერს ჩაწერს ერრორს აჩვენებს
        }else{
         setTasksLoading(true);
         await axios.put(`/profile/updateTask/${id}`, tasks) //ვაგზავნი სერვერზე მონაცემებს რომ შეინახოს ტასკი
         .then((response)=>{
          setSuccess('Updated Succesfully')           //თუ წარმატებით შეინახა ამ სეტყობინებას გამოიტანს
          editTask(id, response.data)
          closeSide(false)
         }).catch((err)=>{
           setError({msg:err.message}) //დაამატებს ერორ მესიჯს თუ სერვერში ვერ შეინახა
         }).finally(() =>setTasksLoading(false))
        }
    }

  return (
    <div>
      <div style={{ width: "500px" }}>
        <img
          style={{ display: "fixed", position: "top" }}
          className="img-fluid mb-5"
          src={Process}
        ></img>

        {error.msg && (
          <div className="alert alert-danger" role="alert">
            <h5>{error.msg}</h5>
          </div>
        )}

        {!error.msg && success && (
          <div className="alert alert-success" role="alert">
            <h5>{success}</h5>
          </div>
        )}

        {tasksLoading ? 
        <div className="spinner-border" style={{marginLeft:'150px', color:'red'}} role="status">
        <span className="visually-hidden ">Loading...</span>
        </div>
        :
        <form onSubmit={onSubmit}>
        <Input
          label="Task Name"
          placeholder="Task"
          type="text"
          className="rainbow-p-around_medium"
          style={inputStyles}
          value={task}
          onChange={(e)=>setTask(e.target.value)}
        />
        <DatePicker
          required
          style={inputStyles}
          type="date"
          value={date}
          label="DatePicker"
          onChange={(date)=>setDate(date)}
        />
        <Select
          label="Status"
          style={inputStyles}
          value={status}
          options={statuses && statuses.statuses?.map((onStatus) => {
            return { value:onStatus, label: onStatus };
          })}
          onChange={(e)=>setStatus(e.target.value)}
          className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
        />
        <div className="row justify-content-center align-content-center">
          <div className="col-9 offset-6">
            <input
              type="submit"
              className="btn btn-primary align-content-center mt-5"
            />
          </div>
        </div>
      </form>
         } 
      </div>
    </div>
  );
}

export default Edittask;
