import React, { useState, useEffect } from "react";
import Tasks from "./tasks";
import "bootstrap/dist/css/bootstrap.min.css";
import Image from "../../images/unnamed.png";
import Sideeffect from "./sideeffect";
import { Application, Avatar, Card } from "react-rainbow-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTasks } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import jwt_decode from "jwt-decode";
import Cookies from "js-cookie";

const iconContainerStyles = {
  width: "2rem",
  height: "2rem",
};

const inputStyles = {
  width: 260,
};

const Profile = () => {

  const [tasks, setTasks] = useState([])
  const [loginData, setLoginData] = useState(jwt_decode(Cookies.get("token")));
  const [statuses, setStatuses] = useState([])
  const [status, setStatus] = useState([])
  const [error, setError] = useState(false)

  useEffect(async ()=>{
      await axios.post(`/profile/getTasks`, {id:loginData.id})
      .then(({data})=>
      setTasks(data)
      ).catch((error)=>{
          setError(error.message)
      })
      
      await axios.get('/status') //იუზეფექტის გამოყენებით ყოველ ჩატვირთვაზე წამოვიღებ მონაცემებს ბაზიდან ტასკის სტატუსებზე
      .then((response) =>{
          if(response.data.length>0){ //თუ წამოღებული ინფორმაციის სიგრძე 0 ზე მეტი იქნება მაშინ შემოდის იფში
              setStatuses(response.data.map(status => status)) //სტატუსებს ვააფდეითებთ და შიგნით ვწერთ ბაზიდან წამოღებულ სტატუსებს
              setStatus(response.data[0])
              // setStatus({status:response.data[0]}) // ხოლო ერთ სტატუსად ვაყენებთ ერეისი არსებულ პირველ სტატუსს
          }
      }).catch(()=>{
        setError({msg:'Server Error 505'})
      })
  },[])

  return (
    <Application>
        {error && (
          <div className="alert alert-danger" role="alert">
            <h5>{error}</h5>
          </div>
        )}
      <section>
        <header className="rainbow-align-content_space-between rainbow-background-color_white rainbow-p-vertical_medium react-rainbow-global-header ml-3">
          <img
            src={Image}
            style={{ width: "300px" }}
            alt="Addon logo"
            className="rainbow-m-left_medium react-rainbow-global-header_logo d-none d-md-block"
          />

          <article className="rainbow-flex rainbow-align_center">
            <Avatar
              src="https://www.addon-solution.de/fileadmin/templates/page.de/img/flameplanred.svg"
              variant="circle"
              className="rainbow-m-horizontal_medium"
            />
          </article>
        </header>
        <section className="rainbow-m-horizontal_large rainbow-m-top_large rainbow-m-bottom_xx-large">
          <Card
            title="Tasks"
            icon={
              <span
                className="rainbow-background-color_brand rainbow-border-radius_circle rainbow-align-content_center"
                style={iconContainerStyles}
              >
                <FontAwesomeIcon
                  icon={faTasks}
                  size="lg"
                  className="rainbow-color_white"
                />
              </span>
            }
            actions={<Sideeffect id='none' type='create' profStatuses={statuses} profStatus={status} setTasks={(task)=>
              setTasks([...tasks, task])
            }/>}
          >
            <div className="rainbow-align-content_center">
            </div>
            <section id="about" className="pt-5">
              <div className="row align-items-center justify-content-between">
                <div className="col-md-12">
                  <Tasks tasks={tasks} statuses={statuses} deleteTask={(id)=>{
                      setTasks(tasks.filter((task)=>
                      {
                          return task._id !==id
                      }
                      ))}
                    } editTask={(id, updatedtask)=>{
                      setTasks(tasks.map((task)=>
                      {
                        return task._id === id ? task = updatedtask : task
                      }
                      ))
                    }}/>
                </div>
              </div>
            </section>
          </Card>
        </section>
      </section>
    </Application>
  );
};

export default Profile;
