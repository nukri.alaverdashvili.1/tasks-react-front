import React, {Component, useState} from 'react';
import { Button, Drawer, ButtonIcon } from 'react-rainbow-components';
import Createtask from './createtask'
import Edittask from './edittask'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPencilAlt,} from '@fortawesome/free-solid-svg-icons';

export default function Sideeffect({id, type, setTasks, editTask, profStatuses, profStatus, profileStatus}) {
    const [isOpenRight, setIsOpenRight] = useState(false)

    const closeSide = (condition) => { 
        setIsOpenRight(condition)
    }
        return (
            <div>
            <div className="rainbow-flex rainbow-flex_row">
                <div>
                    {type == 'create'
                    ?
                    <Button
                        className="rainbow-m-around_medium"
                        label='Create New Task'
                        onClick={() => setIsOpenRight(true)}
                    />
                    :
                    <ButtonIcon
                    tooltip="Edit" 
                    variant="border-filled" 
                    size="medium"
                    icon={<FontAwesomeIcon icon={faPencilAlt}/>}
                    onClick={() => setIsOpenRight(true)}
                    />
                    }
                </div>
            </div>
            <Drawer
                header={type === 'create'?<Createtask closeSide={setIsOpenRight} setTasks={setTasks} profStatuses={profStatuses} profStatus={profStatus}/>:<Edittask id={id} editTask={editTask} closeSide={setIsOpenRight} profileStatus={profileStatus}/>}
                slideFrom="right"
                isOpen={isOpenRight}
                onRequestClose={() => setIsOpenRight(false)}
            />
        </div>
        )
}
