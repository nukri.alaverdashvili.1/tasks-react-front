import React, { Component, useState, useEffect } from "react";
import { DatePicker } from "react-rainbow-components";
import "react-datepicker/dist/react-datepicker.css";
import axios from "axios";
import { Input, Select } from "react-rainbow-components";
import Process from "../../images/Process.svg";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";

const inputStyles = {
  width: 500,
};

function Createtask({closeSide, setTasks, profStatuses, profStatus}) {
  const [task, setTask] = useState("");
  const [date, setDate] = useState(new Date());
  const [status, setStatus] = useState(profStatus);
  const [statuses, setStatuses] = useState(profStatuses);
  const [loginData, setLoginData] = useState(jwt_decode(Cookies.get("token")));
  const [error, setError] = useState([]);
  const [success, setSuccess] = useState(false);
  const [tasksLoading, setTasksLoading] = useState(false);

    async function onSubmit(e)
    {
        e.preventDefault();

        let tasks = {task, date, status}
        if(task == '')
        {
          setError({msg:'TaskName is required'}) //თუ ტასკში არაფერს ჩაწერს ერრორს აჩვენებს
        }else{
          setTasksLoading(true);
         await axios.post(`http://localhost:5000/profile/saveTasks/${loginData.id}`, tasks) //ვაგზავნი სერვერზე მონაცემებს რომ შეინახოს ტასკი
         .then((result)=>{
          setSuccess('Added Succesfully') //თუ წარმატებით შეინახა ამ სეტყობინებას გამოიტანს
          setTasks(result.data)
          closeSide(false)
         }).catch((err)=>{
           setError({msg:err.message}) //დაამატებს ერორ მესიჯს თუ სერვერში ვერ შეინახა
         }).finally(()=>setTasksLoading(false))
        }
    }

  return (
    <div>
      <div style={{ width: "500px" }}>
        <img
          style={{ display: "fixed", position: "top" }}
          className="img-fluid mb-5"
          src={Process}
        ></img>

        {error.msg && (
          <div className="alert alert-danger" role="alert">
            <h5>{error.msg}</h5>
          </div>
        )}

        {!error.msg && success && (
          <div className="alert alert-success" role="alert">
            <h5>{success}</h5>
          </div>
        )}
        {tasksLoading ? 
        <div className="spinner-border" style={{marginLeft:'150px', color:'red'}} role="status">
        <span className="visually-hidden ">Loading...</span>
        </div>
        :
        <form onSubmit={onSubmit}>
          <Input
            label="Task Name"
            placeholder="Task"
            type="text"
            className="rainbow-p-around_medium"
            style={inputStyles}
            value={task}
            onChange={(e)=>setTask(e.target.value)}
          />
          <DatePicker
            required
            style={inputStyles}
            type="date"
            value={date}
            label="DatePicker"
            onChange={(date)=>setDate(date)}
          />
          <Select
            label="Status"
            style={inputStyles}
            options={statuses && statuses.map((onStatus) => {
              return { value:onStatus._id, label: onStatus.status };
            })}
            onChange={(e)=>setStatus(e.target.value)}
            className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
          />
          <div className="row justify-content-center align-content-center">
            <div className="col-9 offset-6">
              <input
                type="submit"
                className="btn btn-primary align-content-center mt-5"
              />
            </div>
          </div>
        </form>
        }
      </div>
    </div>
  );
}

export default Createtask;
