import {React} from 'react'
import "bootstrap/dist/css/bootstrap.min.css"
import GoogleLogin from 'react-google-login'
import './navbar/nav.css';
import Image from '../images/showcase.svg'
import Fundamentals from '../images/show3.jpg'
import { FcUp } from "react-icons/fc";
import Sideregister from './auth/sideregister'
import Sidelogin from './auth/sidelogin'

function Welcome({error, handleLogin,handleFailure}) {
    return (
        <>
        {/* Navbar */}
        <nav className="navbar navbar-expand-lg bg-dark navbar-dark py-3 fixed-top" >    {/* ნავიგაციაში როდესაც lg იქნება ეკრანის ზომა მაშინ გამოჩნდეს მხოლოდ ეს მენიუ. რომელიც ბექგრაუნდი ექნება დარქი */}
        <div className="container">   {/* a ტეგი იქნება კონტეინერში რომელიც გვერდებიდან თანაბრად იქნება დაშორებული */}
          <a href="#" className="navbar-brand"> EgonPlan</a>
    
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navmenutoshow"> <span className="navbar-toggler-icon"></span> </button>{/* დამალული იქნება ეს ბათნი, აიდის მიხედვით ამ ბათნზე დაწოლისას გამოჩნდება მენიუ */}
    
          <div className="collapse navbar-collapse" id="navmenutoshow">
            <ul className="navbar-nav ms-auto"> {/* ms-auto ნიშნავს რომ მარგჯინი სტარტიდან ანუ დაშორება მენიუსგან ავტო იქნება და ბოლოში გადავა მენიუ */}
              <li className="nav-item">
                <a href="#about" className="nav-link">About</a>
              </li>
              <li className="nav-item">
                <a href="#kontakt" className="nav-link">Kontakt</a>
              </li>
              <li className="nav-item">
                <a href="#team" className="nav-link">Team</a>
              </li>
              <li className="nav-item">
                <Sideregister />
              </li>
              <li className="nav-item">
                <Sidelogin />
              </li>
            </ul>
          </div>
        </div>
          </nav> 
        {/* ShowCase */}
        <section className="bg-dark p-5 text-light text-center text-sm-start"> {/* small მდე ტექსტი იქნება სტარტზე ხოლო სმოლის სემთხვევაში გაცენტრდება ტექსტი */}
          <div className="container pt-5">
            <div className="d-sm-flex align-items-center justify-content-between">  {/* ამ დივში არსებულ ელემენტებს თანაბრად გაანაწილებს small ეკრანამდე, ხოლო სმოლზე ფლექსი აღარ იქნება */}
              <div >
                <h1>Ihr Softwarespezialist <span className="text-warning"> für den Energievertrieb </span> </h1> {/* text-warning გააყვითლებს ტექსტს */}
                <p className="lead">
                  Auf Basis der eigenen, unabhängigen Softwarelösung egON entwickeln und integrieren wir intelligente Softwarelösungen für Energievertriebe, 
                  Distributionen und Energieversorger. Wir erkennen die Anforderungen des turbulenten Energiemarktes und setzen sie in messbaren Nutzen für unsere Kunden um. 
                  Durch unsere kontinuierliche Marktrecherche verfügen wir über eine umfangreiche und aktuelle Tarifdatenbank inklusive der entsprechenden Wechsel-Formulare. 
                  Um die automatisierte und sichere Übergabe von Energieaufträgen (Schnittstellen) voranzutreiben pflegen wir den engen technischen Kontakt zu Energieversorgungsunternehmen, 
                  Distributionen und Energiedienstleistern. In enger Zusammenarbeit mit branchenführenden Kunden investieren wir kontinuierlich in Produktinnovationen und die Weiterentwicklung der bestehenden Softwareprodukte.
                </p>
              </div>
              <img className="img-fluid w-50 d-none d-sm-block" src={Image}></img> {/* w-50 იმიჯის ზომას გაანახევრებს d-none ნიშნავს რომ არ გამოჩნდება ფოტო d-sm-block ნიშნავს რომ ფოტო გამოჩნდება სანამ ეკრანი სმოლი იქნება */}
            </div>
          </div>
        </section>
        {/* NewsLetter */}
        <section className="bg-primary text-light p-5">
            <div className="container">
              <div className="d-md-flex justify-content-between align-items-center">  
                <p className="mb-3 mb-md-0">Log in With Google Account, Plan projects, stay on track, and deliver on time without overworking your team.</p>
                {error && <p className='btn-danger mr-5'> Google Auth error 404 : {error} </p>}
                <GoogleLogin 
                    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                    onSuccess={handleLogin}
                    onFailure={handleFailure}
                    cookiePolicy={'single_host_origin'}
                    >
                    </GoogleLogin>
          
              </div>
            </div>
        </section>
    
        {/* LearnSection */}
    
        <section id="about" className="p-5">
          <div className="container">
            <div className="row align-items-center justify-content-between">
                <div className="col-md">
                  <img className="img-fluid" src={Fundamentals}></img>
                </div>
    
                <div className="col-md">
                  <h2>addON Solution GmbH</h2>
                  <p className="lead">
                  Ein starkes und  <span className="text-secondary"> solides Unternehmen </span>
                  </p>
                  <p>
                  addON Solution setzt auf ein stetiges und kontrolliertes Wachstum mit hoher Stabilität. Wir sind zu 100% eigenfinanziert – darauf sind wir stolz. Jeden einzelnen Kunden sehen wir dabei als Fundament unserer Erfolgsgeschichte.
                  </p>
                  <a href="#" className="btn btn-light mt-3">
                  </a>
                </div>
            </div>
          </div>
        </section>
    
            {/* ReactSection */}
    
            <section id="kontakt" className="p-5 bg-dark">
          <div className="container">
            <div className="row align-items-center justify-content-between">
                <div className="col-md text-light">
                  <h2>Anschrift: </h2>
                  <h5>addON-Solution GmbH </h5>
                  <h5>Hans-Knöll-Straße 6 </h5>
                  <h5>07745 Jena</h5>
                </div>
                <div className="col-md text-light">
                  <h2>Kontakt: </h2>
                  <h5> Telefon: 03641-274110 </h5>
                  <h5>E-Mail: info@eg-on.com</h5>
                </div>
                <div className="col-md d-none d-sm-block">
                <div className="mapouter"><div className="gmap_canvas"><iframe width={668} height={500} id="gmap_canvas" src="https://maps.google.com/maps?q=addon-solution&t=&z=13&ie=UTF8&iwloc=&output=embed" frameBorder={0} scrolling="no" marginHeight={0} marginWidth={0} /><br /><style dangerouslySetInnerHTML={{__html: ".mapouter{position:relative;text-align:right;height:500px;width:938px;}" }} /><a href="https://www.embedgooglemap.net">google map websites</a><style dangerouslySetInnerHTML={{__html: ".gmap_canvas {overflow:hidden;background:none!important;height:500px;width:938px;}" }} /></div></div>
                </div>
            </div>
          </div>
        </section>

        {/*  */}
    
        <section id="team" className="p-5 bg-primary" >
          <div className="container">
            <h2 className="text-center text-white">Our Team</h2>

            <div className="row g-4">
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" style={{width:'130px'}} src="https://profile-images.xing.com/images/c3653ffa6abe0bf2bdedc9eb03cdaf4c-3/ingo-faulstich.256x256.jpg"></img>
                        <h3 className="card-title mb-3">Ingo faulstich</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
    
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/men/15.jpg"></img>
                        <h3 className="card-title mb-3">Adam Le</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
                
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/women/11.jpg"></img>
                        <h3 className="card-title mb-3">Anna fran</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
    
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/men/5.jpg"></img>
                        <h3 className="card-title mb-3">Alex Doe</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
            </div>
          </div>
        </section>
    
        {/* Footer */}
        <footer className="p-5 bg-dark text-white text-center position-relative">
        <div className="container">
          <p className="lead">
            Copyright & Copy; 2021 Nukri
          </p>
          <a href="" className="position-absolute bottom-0 end-0 p-5"><FcUp size={40}/></a>
        </div>
        </footer>
        </>
    )
}

export default Welcome
