import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Createtask from "./components/profile/createtask";
import Tasks from "./components/profile/tasks";
import axios from "axios";
import Log from "./components/log";
import Nav from "./components/navbar/nav";
import Profile from "./components/profile/profile";
import Cookies from "js-cookie";

function App() {
  const [loginData, setLoginData] = useState( //ამოწმებს token ს თუ არის Cook ში მაშინ ინახავს ამ ცვლადში, თუარადა ნალს უტოლებს
    Cookies.get("token") ? Cookies.get("token") : null
  );
  const [error, sestError] = useState(false)

  const handleFailure = (result) => {
    sestError(result.error) //თუ გუგლით შესვლისას ერორი დაფიქსირდა, 
  };

  const handleLogin = async (googleData) => {
      await axios.post("/saveUser", { // გუგლ ექაუნთიდან მიღებულ ინფორმაციას ვინახავთ ახალ იუზერში
      googleId: googleData.profileObj.googleId,
      email: googleData.profileObj.email,
      name: googleData.profileObj.name,
    }).then((result)=>{
      Cookies.set("token", result.data.token); // Cook ში ვინახავთ ტოკენს, რომელსაც იუზერის შენახვის შემდეგ სერვერიდან მივიღებთ
      setLoginData(result.data.token);
    }).catch((error)=>{ //თუ ერორი მივიღეთ სერვერიდან ვინახავთ ერორებში
      sestError(error.message) 
    })
  };

  const handleLogout = () => {
    Cookies.remove("token"); //ამ ფუნქციის გამოძახების შემდეგ წაშლის ტოკენს Cook იდან 
    setLoginData(null);
  };
  return (
    <div>
      <Router>
        <Route
          path="/"
          render={() => (
            <>
              <Log
                error={error}
                loginData={loginData}
                handleLogin={handleLogin}
                handleFailure={handleFailure}
              />
            </>
          )}
        />
        <div className="row">
          <div className="col-4">
            {loginData && <Nav logout={handleLogout} />}
          </div>
          <div className="col-8 profile">
            {loginData && (
              <Route
                exact
                path="/profile"
                render={() => (
                  <>
                    <Profile loginData={loginData} />
                  </>
                )}
              />
            )}
            {/* {loginData && <Route path="/create" exact component={Createtask} />}
            {loginData && <Route path="/tasks" exact component={Tasks} />} */}
          </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
